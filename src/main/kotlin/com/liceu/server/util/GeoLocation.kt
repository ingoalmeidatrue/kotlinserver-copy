package com.liceu.server.util

import com.maxmind.geoip2.DatabaseReader
import org.springframework.stereotype.Component
import java.io.File
import java.net.InetAddress

@Component
class GeoLocator {

    final var database: File = File("GeoLite2-City.mmdb")
    var dbReader: DatabaseReader = DatabaseReader.Builder(database).build()

    fun location(ip: String): GeoLocation {
        val inet = InetAddress.getByName(ip)
        val response = dbReader.city(inet)

        val latitude = response.location.latitude
        val longitude = response.location.longitude

        return GeoLocation(
                latitude, longitude
        )
    }
}

data class GeoLocation(
        val latitude: Double,
        val longitude: Double
)