package com.liceu.server.util

import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest

@Component
class NetworkUtils {

    fun networkData(request: HttpServletRequest): Map<String, Any> {
        return mapOf<String, Any>(
                "ip" to request.remoteAddr
        )
    }
}
