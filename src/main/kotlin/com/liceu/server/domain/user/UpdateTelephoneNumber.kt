package com.liceu.server.domain.user

import com.liceu.server.domain.global.*
import com.liceu.server.util.Logging

class UpdateTelephoneNumber(
        private val userRepository: UserBoundary.IRepository,
        private val telephoneNumberCheck: Regex  = "(\\([1-9]{2}\\)|[1-9]{2})9[-\\.]?[0-9]{4}[-\\.]?[0-9]{4}".toRegex(),
        private val removeCharacters: Regex = "[^0-9.]".toRegex()
): UserBoundary.IUpdateTelephoneNumber{
    companion object {
        const val EVENT_NAME = "update_user_telephone_number"
        val TAGS = listOf (USER , UPDATE, TELEPHONE)
    }

    override fun run(userId: String, telephoneNumber: String) {
        try{
            if(telephoneNumber.isBlank()){
                throw UnderflowSizeException("Telephone can't be empty")
            }
            if(!telephoneNumberCheck.matches(telephoneNumber)){
                throw TypeMismatchException("Incorrect form of telephone number")
            }
            val normalizedTelephoneNumber = removeCharacters.replace(telephoneNumber, "").trim()
            Logging.info(EVENT_NAME, TAGS, hashMapOf(
                    "userId" to userId,
                    "telephoneNumber" to normalizedTelephoneNumber
            ))
            userRepository.updateTelephoneNumber(userId,normalizedTelephoneNumber)
        } catch (e: Exception){
            Logging.error(EVENT_NAME, TAGS,e)
            throw e
        }
    }
}