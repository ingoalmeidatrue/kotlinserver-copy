package com.liceu.server.domain.user

import com.liceu.server.domain.challenge.Challenge
import com.liceu.server.domain.global.*
import com.liceu.server.util.Logging
import java.lang.Exception

class ChallengesFromUserId (
        private val repo: UserBoundary.IRepository,
        private val historyAmount: Int
): UserBoundary.IChallengesFromUserById{

    companion object {
        const val EVENT_NAME = "challenges_from_user"
        val TAGS = listOf(RETRIEVAL, CHALLENGE, USER)
    }

    override fun run(userId: String, amount: Int, start: Int): List<Challenge> {
        if(amount == 0) {
            Logging.warn(UNCOMMON_PARAMS, TAGS, hashMapOf(
                    "action" to EVENT_NAME,
                    "value" to amount
            ))
        }
        var finalAmount = amount
        if(amount > historyAmount) {
            finalAmount = historyAmount
            Logging.warn(
                    MAX_RESULTS_OVERFLOW,
                    TAGS + listOf(OVERFLOW),
                    hashMapOf(
                            "requested" to amount,
                            "max_allowed" to historyAmount
                    )
            )
        }
        try {
            val challenges = repo.getChallengesFromUserById(userId,finalAmount,start)
            Logging.info(
                    EVENT_NAME,
                    TAGS,
                    hashMapOf(
                            "userId" to userId
                    ))
            return challenges
        }catch (e: Exception){
            Logging.error(EVENT_NAME, TAGS, e)
            throw e
        }
    }
}