package com.liceu.server.domain.image

class ImageBoundary {
    interface IRepository {
        fun run(fileName: String, imageData: String): String
    }
}