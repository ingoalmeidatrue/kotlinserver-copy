package com.liceu.server.domain.game

import com.liceu.server.domain.global.*
import com.liceu.server.util.Logging
import java.lang.Exception


class GetGames(
    val repo: GameBoundary.IRepository,
    val maxResults: Int
): GameBoundary.IGet {

    companion object {
        const val EVENT_NAME = "user_game"
        val TAGS = listOf(RETRIEVAL, GAME, USER)
    }

    override fun run(userId: String): List<Game> {
        try {
            val games = repo.getGames(userId)
            Logging.info(
                EVENT_NAME,
                TAGS,
                hashMapOf("userId" to userId)
            )
            return games
        } catch (e: Exception) {
            Logging.error(EVENT_NAME, TAGS, e)
            throw e
        }
    }
}