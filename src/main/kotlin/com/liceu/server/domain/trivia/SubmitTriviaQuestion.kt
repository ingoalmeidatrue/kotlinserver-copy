package com.liceu.server.domain.trivia

import com.liceu.server.domain.global.*
import com.liceu.server.util.Logging
import javax.validation.ValidationException

class SubmitTriviaQuestion(
        val triviaRepository: TriviaBoundary.IRepository
): TriviaBoundary.ISubmit {


    companion object {
        const val EVENT_NAME = "trivia_question_submission"
        val TAGS = listOf(INSERTION, TRIVIA, QUESTION)
    }

    override fun run(triviaQuestion: TriviaQuestionSubmission): String {

        try {
            val prohibitedWords = listOf(
                    "porra", "caralho", "merda", "desgraça", "fdp", "filha da puta",
                    "filha da mãe", "vagabunda", "vadia","foda-se", "fudido", "vai se fuder",
                    "fodido"
            )
            if (triviaQuestion.question.isBlank()) throw OverflowSizeException("No question passed")
            if (triviaQuestion.question.length in 301 downTo 19) throw ValidationException("Question length is out of range")
            if(triviaQuestion.correctAnswer.length > 200) throw OverflowSizeException("Too many characters in correct answer")
            if(triviaQuestion.wrongAnswer.length > 200) throw OverflowSizeException("Too many characters in wrong answer")
            if(triviaQuestion.tags.isEmpty()) throw OverflowSizeTagsException("No tags given in trivia question")
            if(triviaQuestion.tags.size > 5) throw OverflowSizeTagsException("Too many tags in trivia question")
            triviaQuestion.tags.forEach {
                if(it.length > 100){
                    throw OverflowSizeException("Too many characters in tags")
                }
            }
            prohibitedWords.forEach {
                if(triviaQuestion.question.toLowerCase().contains(it)){
                    throw InputValidationException("trivia question has prohibited words")
                }
            }

            val id = triviaRepository.insert(TriviaQuestionToInsert(
                    triviaQuestion.userId,
                    triviaQuestion.question,
                    triviaQuestion.correctAnswer,
                    triviaQuestion.wrongAnswer,
                    triviaQuestion.tags
            ))
            Logging.info(EVENT_NAME, TAGS, hashMapOf(
                    "triviaQuestionUserId" to triviaQuestion.userId,
                    "triviaQuestion" to triviaQuestion.question,
                    "triviaQuestionCorrectAnswer" to triviaQuestion.correctAnswer,
                    "triviaQuestionWrongAnswer" to triviaQuestion.wrongAnswer,
                    "triviaQuestionTags" to triviaQuestion.tags,
                    "triviaQuestionTagsSize" to triviaQuestion.tags.size,
                    "triviaQuestionBodySize" to triviaQuestion.question.length
                    ))
            return id
        }
        catch (e: Exception){
            Logging.error(EVENT_NAME, TAGS, e)
            throw e
        }
    }
}