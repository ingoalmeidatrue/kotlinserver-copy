package com.liceu.server.domain.util.updateRottenProfileImage

import com.google.api.client.util.IOUtils
import com.liceu.server.domain.image.ImageBoundary
import com.liceu.server.domain.user.UserBoundary
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.URL
import java.util.*
import kotlin.concurrent.thread

fun updateRottenProfileImage(userRepo: UserBoundary.IRepository, bucketUpload: ImageBoundary.IRepository, pictureUrl: String, userId: String) {
    thread(start = true, name = "updateLoginImage") {
        val base64ImageString = encoder(pictureUrl)
        val newPicURL = bucketUpload.run(userId, base64ImageString)
        userRepo.updateProfileImage(userId, newPicURL)
    }
}

fun encoder(filePath: String): String {
    val bytes = downloadFile(URL(filePath)) ?: return ""
    val base64 = Base64.getEncoder().encodeToString(bytes)
    return "data:image/png;base64,$base64"
}

fun downloadFile(url: URL): ByteArray? {
    try {
        val conn = url.openConnection();
        conn.setConnectTimeout(5000);
        conn.setReadTimeout(5000);
        conn.connect();

        val baos = ByteArrayOutputStream();
        IOUtils.copy(conn.getInputStream(), baos)
        return baos.toByteArray();
    } catch (e: IOException) {
        e.printStackTrace();
    }
    return null
}




