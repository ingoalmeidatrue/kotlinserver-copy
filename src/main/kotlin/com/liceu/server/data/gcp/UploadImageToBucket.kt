package com.liceu.server.data.gcp

import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.storage.BlobId
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.StorageOptions
import com.liceu.server.domain.global.*
import com.liceu.server.domain.image.ImageBoundary
import com.liceu.server.domain.util.fileFunctions.FileFunctions
import java.io.FileInputStream
import java.util.*

class UploadImageToBucket(private val bucketName: String) : ImageBoundary.IRepository {

    //bucket connection
    val storage = StorageOptions.newBuilder()
            .setCredentials(ServiceAccountCredentials.fromStream(FileInputStream("googleImagesLiceu.json")))
            .build()
            .service

    override fun run(userId: String, imageData: String): String {
        val imageTypes = hashMapOf(
                "image/jpeg" to "jpeg",
                "image/png" to "png"
        )
        if (imageData.isBlank()) {
            throw UnderflowSizeException("ImageData can't be empty")
        }
        var fileExtension = FileFunctions.extractMimeType(imageData)
        if (fileExtension.isEmpty()) {
            throw TypeMismatchException("this file type is not supported")
        }
        //verifying size of archive
        val formattedEncryptedBytes = imageData.substringAfterLast(",")
        if (FileFunctions.calculateFileSize(formattedEncryptedBytes) > 1e7) {
            throw OverflowSizeException("image size is greater than 10MB")
        }

        //decoding and retrieving image type
        var contentType = fileExtension
        imageTypes.forEach {
            fileExtension = fileExtension.replace(it.key, it.value)
        }

        var fileName = "${userId}.${fileExtension}"
        val imageByteArray = Base64.getDecoder().decode(formattedEncryptedBytes)

        val blobId = BlobId.of(bucketName, fileName)
        val blobInfo = BlobInfo.newBuilder(blobId).setContentType(contentType).build()
        storage.create(blobInfo, imageByteArray)
        val urlLink = "https://storage.googleapis.com/${bucketName}/${fileName}"

        return urlLink;
    }
}