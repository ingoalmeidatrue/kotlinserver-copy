package com.liceu.server.data.search

import com.liceu.server.domain.global.FALLBACK
import com.liceu.server.domain.global.FINDER
import com.liceu.server.domain.global.USER
import com.liceu.server.domain.user.User
import com.liceu.server.domain.user.UserBoundary
import com.liceu.server.util.Logging
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserSearchRepository: UserBoundary.ISearch {


    @Autowired
    lateinit var userRepository: UserBoundary.IRepository
    @Autowired
    lateinit var restHighLevelClient: RestHighLevelClient

    @HystrixCommand(fallbackMethod = "mongoDbSearchFinder")
    override fun run(nameSearched: String, amount: Int): List<User> {
        val sourceBuilder = SearchSourceBuilder()
        sourceBuilder.query(QueryBuilders.matchQuery("name", nameSearched))
        sourceBuilder.fetchSource("_id",null)
        sourceBuilder.size(amount)

        val searchRequest = SearchRequest()
        searchRequest.indices("liceu_users")
        searchRequest.source(sourceBuilder)
        val search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT)

        val obj = JSONObject(search)
        var idsFromSearch : MutableList<String> = arrayListOf()
        val returnLength = obj.getJSONObject("hits").getJSONArray("hits").length()
        for (i in 0 until returnLength){
            idsFromSearch.add(obj.getJSONObject("hits").getJSONArray("hits").getJSONObject(i).getString("id"))
        }
        return userRepository.getMultipleUsersFromIds(idsFromSearch,amount)
    }

    fun mongoDbSearchFinder (nameSearched: String,amount: Int): List<User> {
        Logging.warn("user_search_fallback", listOf(FALLBACK, USER, FINDER), hashMapOf(
                "nameSearched" to nameSearched,
                "amount" to amount
        ))
        return userRepository.getUsersByNameUsingLocation(nameSearched,amount)

    }

}