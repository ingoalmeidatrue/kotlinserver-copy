package com.liceu.server.presentation

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
class BaseController {

    @RequestMapping("/")
    fun get(): String {
        return "Hello World"
    }

}