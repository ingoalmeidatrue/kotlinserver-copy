package com.liceu.server

import com.liceu.server.data.*
import com.liceu.server.data.firebase.FirebaseNotifications
import com.liceu.server.data.gcp.UploadImageToBucket
import com.liceu.server.data.search.SearchRepository
import com.liceu.server.data.search.UserSearchRepository
import com.liceu.server.domain.activities.ActivityBoundary
import com.liceu.server.domain.activities.GetActivitiesFromUser
import com.liceu.server.domain.challenge.*
import com.liceu.server.domain.game.GameBoundary
import com.liceu.server.domain.game.GameRanking
import com.liceu.server.domain.game.SubmitGame
import com.liceu.server.domain.post.*
import com.liceu.server.domain.question.QuestionBoundary
import com.liceu.server.domain.question.QuestionById
import com.liceu.server.domain.question.RandomQuestions
import com.liceu.server.domain.question.QuestionVideos
import com.liceu.server.domain.trivia.*
import com.liceu.server.domain.user.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.boot.web.servlet.ServletComponentScan


@Configuration
@ServletComponentScan
class Beans {

    @Value("\${mongo.uri}")
    lateinit var mongoURI: String

    @Value("\${mongo.dbName}")
    lateinit var mongoDBName: String

    @Value("\${google.profileBucket}")
    lateinit var googleProfileImageBucket: String

    @Value("\${google.imageBucket}")
    lateinit var googleImageBucket: String

    @Value("\${google.documentBucket}")
    lateinit var googleDocumentBucket: String

    @Value("\${values.triviaAmount}")
    var triviaAmount: Int = 4

    @Value("\${values.challengeHistoryAmount}")
    var challengeHistoryAmount: Int = 10

    @Value("\${slack.slackReportWebhook}")
    lateinit var reportWebhookURL: String

    @Value("\${values.reportTagsAmount}")
    var reportTagsAmount: Int = 30

    @Value("\${values.reportParamsAmount}")
    var reportParamsAmount: Int = 30

    @Value("\${values.reportMessageLength}")
    var reportMessageLength: Int = 200

    @Value("\${values.postsNumberApproval}")
    var postsNumberApproval: Int = 10

    @Value("\${values.postFinderAmount}")
    var postFinderAmount: Int = 20

    @Value("\${values.postSavedAmount}")
    var postSavedAmount: Int = 20

    @Autowired
    lateinit var mongoQuestionRepository: MongoQuestionRepository
    @Autowired
    lateinit var mongoUserRepository: MongoUserRepository

    @Autowired
    lateinit var mongoGameRepository: MongoGameRepository

    @Autowired
    lateinit var mongoTriviaRepository: MongoTriviaRepository

    @Autowired
    lateinit var mongoChallengeRepository: MongoChallengeRepository

    @Autowired
    lateinit var mongoPostRepository: MongoPostRepository

    @Autowired
    lateinit var mongoActivityRepository: MongoActivityRepository

    @Autowired
    lateinit var facebookAPI: FacebookAPI

    @Autowired
    lateinit var googleAPI: GoogleAPI

    @Autowired
    lateinit var firebaseNotifications: FirebaseNotifications

    @Autowired
    lateinit var elasticSearchFinder: SearchRepository

    @Autowired
    lateinit var elasticSearchUserFinder: UserSearchRepository

    val profilePicBucket by lazy {
        UploadImageToBucket(googleProfileImageBucket)
    }

    @Bean
    fun random(): QuestionBoundary.IRandom {
        return RandomQuestions(mongoQuestionRepository, 30)
    }

    @Bean
    fun videos(): QuestionBoundary.IVideos {
        return QuestionVideos(mongoQuestionRepository, 30)
    }

    @Bean
    fun multipleAuthenticate(): UserBoundary.IMultipleAuthenticate {
        return MultipleAuthenticate(mongoUserRepository, facebookAPI, googleAPI, profilePicBucket)
    }

    @Bean
    fun submitGame(): GameBoundary.ISubmit {
        return SubmitGame(mongoGameRepository)
    }

    @Bean
    fun ranking(): GameBoundary.IGameRanking {
        return GameRanking(mongoGameRepository, 50)
    }

    @Bean
    fun getUserById(): UserBoundary.IUserById {
        return UserById(mongoUserRepository)
    }

    @Bean
    fun getUsersByNameFromLocation(): UserBoundary.IGetUsersByNameUsingLocation {
        return UsersByNameUsingLocation(elasticSearchUserFinder, 30)
    }

    @Bean
    fun getChallengesFromUserById(): UserBoundary.IChallengesFromUserById {
        return ChallengesFromUserId(mongoUserRepository, challengeHistoryAmount)
    }

    @Bean
    fun updateLocationFromUser(): UserBoundary.IUpdateLocation {
        return UpdateLocation(mongoUserRepository)
    }

    @Bean
    fun updateSchoolFromUser(): UserBoundary.IUpdateSchool {
        return UpdateSchool(mongoUserRepository)
    }

    @Bean
    fun updateAge(): UserBoundary.IUpdateAge {
        return UpdateAge(mongoUserRepository)
    }

    @Bean
    fun updateYoutubeChannel(): UserBoundary.IUpdateYoutubeChannel {
        return UpdateYoutubeChannel(mongoUserRepository)
    }

    @Bean
    fun updateInstagramProfile(): UserBoundary.IUpdateInstagramProfile {
        return UpdateInstagramProfile(mongoUserRepository)
    }

    @Bean
    fun updateDescription(): UserBoundary.IUpdateDescription {
        return UpdateDescription(mongoUserRepository)
    }

    @Bean
    fun updateWebsite(): UserBoundary.IUpdateWebsite {
        return UpdateWebsite(mongoUserRepository)
    }

    @Bean
    fun updateProducerToBeFollowed(): UserBoundary.IUpdateProducerToBeFollowed {
        return UpdateProducerToBeFollowed(mongoUserRepository, mongoActivityRepository)
    }

    @Bean
    fun updateProducerToBeUnfollowed(): UserBoundary.IUpdateProducerToBeUnfollowed {
        return UpdateProducerToBeUnfollowed(mongoUserRepository)
    }

    @Bean
    fun updateProfileImage(): UserBoundary.IUpdateProfileImage {
        return UpdateProfileImage(mongoUserRepository, googleImageBucket)
    }

    @Bean
    fun updateFcmToken(): UserBoundary.IUpdateFcmToken {
        return UpdateFcmToken(mongoUserRepository)
    }

    @Bean
    fun UpdateControlInformation(): UserBoundary.IUpdateControlInformation {
        return UpdateControlInformation(mongoUserRepository, mongoActivityRepository)
    }

    @Bean
    fun updateDesiredCourse(): UserBoundary.IUpdateCourse {
        return UpdateDesiredCourse(mongoUserRepository)
    }

    @Bean
    fun updateTelephoneNumber(): UserBoundary.IUpdateTelephoneNumber {
        return UpdateTelephoneNumber(mongoUserRepository)
    }

    @Bean
    fun updateAddToSavedPosts(): UserBoundary.IUpdatePostToBeSaved {
        return UpdatePostToBeSaved(mongoUserRepository, mongoPostRepository)
    }

    @Bean
    fun updateRemoveFromSavedPosts(): UserBoundary.IUpdateSavedPostToBeRemoved {
        return UpdatePostSavedToBeRemoved(mongoUserRepository)
    }

    @Bean
    fun getPostsSaved(): UserBoundary.IGetSavedPosts {
        return GetPostsSaved(mongoUserRepository, postSavedAmount)
    }

    @Bean
    fun getQuestionById(): QuestionBoundary.IQuestionById {
        return QuestionById(mongoQuestionRepository)
    }

    @Bean
    fun submitTriviaQuestion(): TriviaBoundary.ISubmit {
        return SubmitTriviaQuestion(mongoTriviaRepository)
    }

    @Bean
    fun randomTriviaQuestions(): TriviaBoundary.IRandomQuestions {
        return TriviaRandomQuestions(mongoTriviaRepository, 5)
    }

    @Bean
    fun updateCommentsTrivia(): TriviaBoundary.IUpdateListOfComments {
        return UpdateCommentsTrivia(mongoTriviaRepository, mongoUserRepository)
    }

    @Bean
    fun updateRatingTrivia(): TriviaBoundary.IUpdateRating {
        return UpdateRating(mongoTriviaRepository)
    }

    @Bean
    fun submitChallenge(): ChallengeBoundary.ICreateChallenge {
        return SubmitChallenge(mongoChallengeRepository, mongoTriviaRepository, mongoActivityRepository, mongoUserRepository, firebaseNotifications, triviaAmount)
    }

    @Bean
    fun getChallenge(): ChallengeBoundary.IGetChallenge {
        return GetChallenge(mongoChallengeRepository, mongoTriviaRepository, mongoActivityRepository, mongoUserRepository, firebaseNotifications, triviaAmount)
    }

    @Bean
    fun getDirectChallenge(): ChallengeBoundary.IAcceptDirectChallenge {
        return AcceptDirectChallenge(mongoChallengeRepository, mongoActivityRepository)
    }

    @Bean
    fun UpdateAnswers(): ChallengeBoundary.IUpdateAnswers {
        return UpdateAnswers(mongoChallengeRepository, mongoActivityRepository, mongoUserRepository, firebaseNotifications)
    }

    @Bean
    fun imagePost(): PostBoundary.IImagePost {
        return ImagePost(mongoPostRepository, googleImageBucket, mongoUserRepository, postsNumberApproval)
    }

    @Bean
    fun multipleImagesPost(): PostBoundary.IMultipleImagesPosts {
        return MultipleImagesPost(mongoPostRepository, googleImageBucket, mongoUserRepository, postsNumberApproval)
    }

    @Bean
    fun videoPost(): PostBoundary.IVideoPost {
        return VideoPost(mongoPostRepository, mongoUserRepository, postsNumberApproval)
    }

    @Bean
    fun getPosts(): PostBoundary.IGetPosts {
        return GetPosts(mongoPostRepository, mongoUserRepository, 30)
    }

    @Bean
    fun getPostsFromUser(): PostBoundary.IGetPostsFromUser {
        return GetPostsFromUser(mongoPostRepository)
    }

    @Bean
    fun getRandomPosts(): PostBoundary.IGetRandomPosts {
        return GetRandomPosts(mongoPostRepository, 20)
    }

    @Bean
    fun getPostById(): PostBoundary.IGetPostById {
        return GetPostById(mongoPostRepository)
    }

    @Bean
    fun getPostByDescription(): PostBoundary.IGetPostsByDescription {
        return GetPostsByDescription(postFinderAmount, elasticSearchFinder)
    }

    @Bean
    fun updateComments(): PostBoundary.IUpdateListOfComments {
        return UpdateComments(mongoPostRepository, mongoUserRepository)
    }

    @Bean
    fun updateDocument(): PostBoundary.IUpdateDocument {
        return UpdateDocument(mongoPostRepository, googleDocumentBucket)
    }

    @Bean
    fun updatePostRating(): PostBoundary.IUpdateRating {
        return UpdatePostRating(mongoPostRepository)
    }

    @Bean
    fun deletePost(): PostBoundary.IDeletePost {
        return DeletePosts(mongoPostRepository)
    }

    @Bean
    fun deleteCommentPost(): PostBoundary.IDeleteCommentPost {
        return DeleteCommentPost(mongoPostRepository)
    }

    @Bean
    fun getActivitiesFromUser(): ActivityBoundary.IGetActivitiesFromUser {
        return GetActivitiesFromUser(mongoActivityRepository, 50)
    }
}